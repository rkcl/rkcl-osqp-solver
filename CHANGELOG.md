# [](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/compare/v2.0.0...v) (2022-05-13)


### Bug Fixes

* add return for configure fct ([4bb2058](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/commits/4bb20584186ee647f86fc2a719c3ec28ca1e72a6))


### Features

* allow to configure in a separate fct ([aec9e97](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/commits/aec9e97f2e0e41041078bdefe39ce450069a3579))
* update dep to rkcl-core ([3f02f9b](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/commits/3f02f9bceeafce9717bfef2e9fa84482b921395d))



# [2.0.0](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/compare/v1.1.0...v2.0.0) (2021-10-01)


### Features

* use conventional commits ([346b425](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/commits/346b4258ed4822b5435454eabdbf7b86229a5d42))



# [1.1.0](https://gite.lirmm.fr/rkcl/rkcl-osqp-solver/compare/v1.0.1...v1.1.0) (2020-10-14)



## 1.0.1 (2020-02-24)



